# Studentska Sluzba - REST API

## Running Studentska Sluzba

Studentska sluzba is a Spring Boot application built using Maven.

You can run the application using STS or any other IDE (NetBeans, Eclipse, IntelliJ IDEA).

1) STS 

File -> Import -> Maven -> Existing Maven project

After importing, right click on project -> Run AS -> Spring Boot App

2) Eclipse 

File -> Import -> Maven -> Existing Maven project

After importing, right click on project -> Run AS -> Maven build, Goals: spring-boot:run

3) IntelliJ IDEA 

From main menu choose Import Project, select studentska-sluzba. 

Run the application by right clicking on the StudentskaSluzbaApplication main class and choosing Run 'StudentskaSluzbaApplication'


## Database configuration

Studentska sluzba app uses MySQL database. Before starting application you should make new database called db_studentska_sluzba. After that you can run the app and all tables will generate automatically. Now you can insert new data into database. Inside SQL folder is test data.

Studentska sluzba can use any database. It's important to add maven dependency for specific database and change application.propertise file.

## Postman

In the studentska-sluzba folder you can find StudentskaSluzbaRest.postman_collection file.

Open Postman application and import this collection.

In order to use application (login), you need to have appropriate user (username, password, rights).

In SQL data file you can find two users with different authorities. Passwords are saved encripted (Bcrypt function is used for encription).
If you want to add new user in database, you can generate new password for user using Bcrypt generator (https://bcrypt-generator.com/).

On login request, token which will be generated, will be used in every other request.
In requests headers you will find key with name "X-Auth-Token", you need to copy generated token in the value field to send request correctly.




