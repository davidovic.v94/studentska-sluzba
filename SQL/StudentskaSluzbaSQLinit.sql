
use db_studentska_sluzba;

insert into user (password, username) values('$2y$12$rAybz7taAsOt4AjFt1H3reY8MKwcSIRW/YGEqdbPwcalAXV0vBcYK', 'admin');
#pass = 12345
insert into user (password, username) values('$2y$12$H7Nkds3ESMf5CjxTNbw3keVi67epDojzgv5.5Div9/Lps3dqkz8Q.', 'user');
#pass = 11111



insert into authority (name) values('ROLE_USER');
insert into authority (name) values('ROLE_ADMIN');
insert into authority (name) values('ROLE_PROFESSOR');
insert into authority (name) values('ROLE_STUDENT');



insert into user_authority (authority_id, user_id) values(2, 1);
insert into user_authority (authority_id, user_id) values(4, 1);
insert into user_authority (authority_id, user_id) values(1, 2);


insert into student (username, first_name, last_name, birth, address, email, program_id, year, card_number) 
	values('vojkan', 'Vojislav', 'Davidovic', '1994-09-17', 'BJ 112v', 'vojkan@gmail.com', 3, 1, 'e123');
insert into student (username, first_name, last_name, birth, address, email, program_id, year, card_number) 
	values('nikola', 'Nikola', 'Nikolic', '1994-09-12', 'Crnotravska 8', 'nikola@gmail.com', 5, 1, 'e555');
insert into student (username, first_name, last_name, birth, address, email, program, year, card_number) 
	values('1991-02-14', 'e111',' milos@gmail.com', 'Milos', 'Milosevic', 'racunarstvo', 1);
insert into student (birth, card_number, email, first_name, last_name, program, year) 
	values('1993-09-10', 'e222',' marko@gmail.com', 'Marko', 'Markovic', 'racunarstvo', 1);
insert into student (birth, card_number, email, first_name, last_name, program, year) 
	values('1994-03-12', 'e333',' mirko@gmail.com', 'Mirko', 'Mirkovic', 'racunarstvo', 2);
insert into student (birth, card_number, email, first_name, last_name, program, year) 
	values('1991-05-19', 'e444',' petar@gmail.com', 'Petar', 'Petrovic', 'telekomunikacije', 4);
insert into student (birth, card_number, email, first_name, last_name, program, year) 
	values('1994-09-12', 'e555',' nikola@gmail.com', 'Nikola', 'Nikolic', 'telekomunikacije', 2);
insert into student (username, birth, card_number, email, first_name, last_name, address, program_id, year) 
	values('jovan', '1992-02-05', 'e666',' jovan@gmail.com', 'Jovan', 'Jovanovic', 'Crnotravska 83', 1, 3);
insert into student (username, birth, card_number, email, first_name, last_name, address, program_id, year) 
	values('ivan', '1993-09-28', 'e777',' ivan@gmail.com', 'Ivan', 'Ivanovic', 'Cvijiceva 6', 1, 3);
insert into student (username, birth, card_number, email, first_name, last_name, address, program_id, year) 
	values('ilija', '1995-01-11', 'e888',' ilija@gmail.com', 'Ilija', 'Ilic', 'Savska 125', 4, 2);
insert into student (username, birth, card_number, email, first_name, last_name, address, program_id, year) 
	values('novak', '1995-02-09', 'e999',' novak@gmail.com', 'Novak', 'Novakovic', 'Mestroviceva 10', 5, 2);
insert into student (username, birth, card_number, email, first_name, last_name, address, program_id, year) 
	values('rajko', '1994-04-08', 'e100',' rajko@gmail.com', 'Rajko', 'Rajkovic', 'Francuska 27', 5, 3);
    
insert into professor (username, first_name, last_name, title, birth, address, email) 
	values('marko', 'Milovan', 'Milovanovic', 'full professor', '1967-04-08', 'Terazije 10A', 'profesor1@gmail.com');
insert into professor (birth, email, first_name, last_name, title, address, username) 
	values('1982-05-22', 'profesor2@gmail.com', 'Nemanja', 'Jovic', 'assistant professor', 'Terazije 18c', 'nemanja');
insert into professor (birth, email, first_name, last_name, title, address, username) 
	values('1975-12-05', 'profesor3@gmail.com', 'Stanko', 'Milic', 'teaching assistant', 'Peke Pavlovica 32', 'stanko');
insert into professor (birth, email, first_name, last_name, title, address, username) 
	values('1962-02-15', 'profesor4@gmail.com', 'Jovica', 'Milijas', 'full professor', 'Cvijiceva 14', 'jovica');
insert into professor (birth, email, first_name, last_name, title, address, username) 
	values('1977-03-11', 'profesor5@gmail.com', 'Dragan', 'Savic', 'assistant professor', 'Nikole Pasica bb', 'dragan');
insert into professor (birth, email, first_name, last_name, title, address, username) 
	values('1958-11-23', 'profesor6@gmail.com', 'Stojan', 'Milunovic', 'full professor', 'Svetosavska 3', 'stojan');
insert into professor (birth, email, first_name, last_name, title, address, username) 
	values('1985-04-17', 'profesor7@gmail.com', 'Radovan', 'Petrovic', 'full professor', 'Bircaninova 19', 'radovan');
    
    
    
insert into course (code, espb, name, semester, professor_id) 
	values('ma1', 6, 'Matematika 1', 1, 1);
insert into course (code, espb, name, semester, professor_id) 
	values('ma2', 6, 'Matematika 2', 2, 4);
insert into course (code, espb, name, semester, professor_id) 
	values('ma3', 6, 'Matematika 3', 3, 2);
insert into course (code, espb, name, semester, professor_id) 
	values('ma4', 5, 'Matematika 4', 4, 5);
insert into course (code, espb, name, semester, professor_id) 
	values('fk1', 5, 'Fizika 1', 1, 6);
insert into course (code, espb, name, semester, professor_id) 
	values('fk2', 6, 'Fizika 2', 2, 6);
insert into course (code, espb, name, semester, professor_id) 
	values('tk1', 7, 'Telekomunikacije 1', 3, 3);
insert into course (code, espb, name, semester, professor_id) 
	values('tk2', 5, 'Telekomunikacije 2', 4, 3);
insert into course (code, espb, name, semester, professor_id) 
	values('oop', 6, 'OOP', 5, 7);
insert into course (code, espb, name, semester, professor_id) 
	values('pr1', 6, 'Programiranje 1', 6, 7);
insert into course (code, espb, name, semester, professor_id) 
	values('bp2', 5, 'Baze Podataka 2', 7, 7);
insert into course (code, espb, name, semester, professor_id) 
	values('pis1', 5, 'Projektovanje informacionih sistema', 8, 7);
    

    
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-23', 6, 1, 1, 1);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-23', 7, 1, 2, 1);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-23', 8, 1, 3, 1);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-23', 8, 1, 4, 1);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-25', 6, 2, 5, 4);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-25', 7, 2, 6, 4);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-25', 8, 2, 7, 4);
insert into exam (date, grade, course_id, student_id, professor_id) 
	values('2020-01-25', 8, 2, 8, 5);

    
