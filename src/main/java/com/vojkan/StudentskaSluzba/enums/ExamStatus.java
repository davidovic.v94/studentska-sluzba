package com.vojkan.StudentskaSluzba.enums;

public enum ExamStatus {
	PASSED,
    NOT_PASSED,
    REGISTERED,
    NOT_REGISTERED
}
