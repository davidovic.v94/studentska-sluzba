package com.vojkan.StudentskaSluzba.exceptions;

import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javassist.NotFoundException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		CustomExceptionModel exceptionModel = new CustomExceptionModel();
		
		exceptionModel.setTimestamp(new Date());
		exceptionModel.setStatus(status.value());
		exceptionModel.setErrors(ex.getBindingResult()
								   .getFieldErrors()
								   .stream()
								   .map(x -> x.getDefaultMessage())
								   .collect(Collectors.toList()));
		
		return new ResponseEntity<Object>(exceptionModel, headers, status);
		
	}	
	
	@ExceptionHandler(ConstraintViolationException.class)
	public void handleConstraintViolationException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}
	
	@ExceptionHandler(SQLIntegrityConstraintViolationException.class)
	public void handleSQLIntegrityConstraintViolationException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}
	
	@ExceptionHandler(NotFoundException.class)
	public void handleNotFoundException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value());
	}
	
	@ExceptionHandler(ParseException.class)
	public void handleParseException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}
	
	@ExceptionHandler(Exception.class)
	public void handleException(HttpServletResponse response) throws Exception {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}
	
}
