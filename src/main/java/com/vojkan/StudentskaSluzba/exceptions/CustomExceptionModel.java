package com.vojkan.StudentskaSluzba.exceptions;

import java.util.Date;
import java.util.List;

public class CustomExceptionModel {

	private Date timestamp;
	private int status;
	private List<String> errors;
	
	public CustomExceptionModel() {
		
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public List<String> getErrors() {
		return errors;
	}
	
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	 	
}
