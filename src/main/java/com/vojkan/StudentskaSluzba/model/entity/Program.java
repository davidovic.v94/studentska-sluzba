package com.vojkan.StudentskaSluzba.model.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Program {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true)
	private String name;
	
	@OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Course> courses = new HashSet<Course>();
	
	@OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Student> students = new HashSet<Student>();
	
	private int totalEspb;
	
	public Program() {
		
	}
	
	public Program(Long id, String name, int totalEspb) {
		super();
		this.id = id;
		this.name = name;
		this.totalEspb = totalEspb;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public int getTotalEspb() {
		return totalEspb;
	}

	public void setTotalEspb(int totalEspb) {
		this.totalEspb = totalEspb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Program other = (Program) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
	
}
