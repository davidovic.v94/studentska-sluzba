package com.vojkan.StudentskaSluzba.model.dto;

import java.util.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.vojkan.StudentskaSluzba.validation.ValidCourse;
import com.vojkan.StudentskaSluzba.validation.ValidProfessor;
import com.vojkan.StudentskaSluzba.validation.ValidStudent;

public class ExamDTO {

	private Long id;
	
	@ValidStudent
	private StudentDTO student;
		
	@ValidCourse
	private CourseDTO course;		
	
	@NotNull(message = "Date is required!")
	@PastOrPresent(message = "Date must be in format: dd.MM.yyyy. (past or present!)")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy.", timezone = "Europe/Belgrade")
	private Date date;

//	@NotNull(message = "Grade is required!")
//	@Min(value = 5,  message = "Lowest grade is 5!")
//	@Max(value = 10, message = "Highest grade is 10!")
//	@Digits(fraction = 0, integer = 2, message = "Grade must be between 5 and 10!")
	private int grade;
	
	@NotNull(message = "Points are required!")
	@Min(value = 0,  message = "Lowest number of points is 0!")
	@Max(value = 100, message = "Highest number of points is 100!")
	@Digits(fraction = 0, integer = 3, message = "Number of points must be between 0 and 100!")
	private int points;
	
//	@NotNull(message = "Exam status is required!")
	private boolean passed;

	@ValidProfessor
	private ProfessorDTO professor;
	
	public ExamDTO() {
		
	}

	public ExamDTO(Long id, StudentDTO student, CourseDTO course, Date date,
			ProfessorDTO professor, int points) {
		super();
		this.id = id;
		this.student = student;
		this.course = course;
		this.date = date;
		this.grade = 5;
		this.professor = professor;
		this.points = points;
		this.passed = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public CourseDTO getCourse() {
		return course;
	}

	public void setCourse(CourseDTO course) {
		this.course = course;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public ProfessorDTO getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorDTO professor) {
		this.professor = professor;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public boolean isPassed() {
		return passed;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}
	
}
