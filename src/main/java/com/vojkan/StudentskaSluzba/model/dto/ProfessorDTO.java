package com.vojkan.StudentskaSluzba.model.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class ProfessorDTO {

	private Long id;	
	
	private String username;

	@NotEmpty(message = "First name is required!")
	@Size(min = 1, max = 20, message = "First name must be minimum one character!")
	private String firstName;

	@NotEmpty(message = "Last name is required!")
	@Size(min = 1, max = 20, message = "Last name must be minimum one character!")
	private String lastName;
	
	@NotNull(message = "Birth date is required!")
	@Past(message = "Invalid birth date!")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy", timezone = "Europe/Belgrade")
	private Date birth;
	
	private String address;

	@NotEmpty(message = "Title is required!")
	@Size(min = 3, max = 20, message = "Title must be minimum five character!")
	private String title;

	@NotEmpty(message = "Email is required!")
	@Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Email format: user@domain.com")
	private String email;
	
	private List<String> roles = new ArrayList<String>();

	@JsonIgnore
	private List<ExamDTO> exams = new ArrayList<ExamDTO>();
	
	@JsonIgnore
	private List<CourseDTO> courses = new ArrayList<CourseDTO>();
	
	public ProfessorDTO() {
		
	}

	public ProfessorDTO(Long id, String username, String firstName, String lastName, Date birth, String address, String title, String email) {
		super();
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birth = birth;
		this.address = address;
		this.title = title;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<CourseDTO> getCourses() {
		return courses;
	}

	public void setCourses(List<CourseDTO> courses) {
		this.courses = courses;
	}

	public List<ExamDTO> getExams() {
		return exams;
	}

	public void setExams(List<ExamDTO> exams) {
		this.exams = exams;
	}
	
}
