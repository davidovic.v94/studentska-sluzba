package com.vojkan.StudentskaSluzba.model.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProgramDTO {
	
	private Long id;
	
	@NotEmpty(message = "Name is required!")
	@Size(min = 3, max = 50, message = "Program name must be three or more characters!")
	private String name;
	
	@JsonIgnore
	private List<CourseDTO> courses = new ArrayList<CourseDTO>();
	
	@JsonIgnore
	private List<StudentDTO> students = new ArrayList<StudentDTO>();

	@NotNull(message = "Espb is required!")
	private int totalEspb;
	
	public ProgramDTO() {
		
	}
	
	public ProgramDTO(Long id, String name, int totalEspb) {
		super();
		this.id = id;
		this.name = name;
		this.totalEspb = totalEspb;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CourseDTO> getCourses() {
		return courses;
	}

	public void setCourses(List<CourseDTO> courses) {
		this.courses = courses;
	}

	public List<StudentDTO> getStudents() {
		return students;
	}

	public void setStudents(List<StudentDTO> students) {
		this.students = students;
	}

	public int getTotalEspb() {
		return totalEspb;
	}

	public void setTotalEspb(int totalEspb) {
		this.totalEspb = totalEspb;
	}	
 
}
