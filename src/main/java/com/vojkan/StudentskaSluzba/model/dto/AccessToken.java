package com.vojkan.StudentskaSluzba.model.dto;

public class AccessToken {

	private String token;
	private Long expiresAt;
	private Object user;
	
	public AccessToken() {
		
	}
	
	public AccessToken(String token, Long expiresAt, Object user) {
		super();
		this.token = token;
		this.expiresAt = expiresAt;
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Long expiresAt) {
		this.expiresAt = expiresAt;
	}

	public Object getUser() {
		return user;
	}

	public void setUser(Object user) {
		this.user = user;
	}	
	
}
