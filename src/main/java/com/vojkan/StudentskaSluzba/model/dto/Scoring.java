package com.vojkan.StudentskaSluzba.model.dto;

public class Scoring {
	
	private int firstColloquium;
	private int secondColloquium;
	private int finalExam;
	private int total;
	
	public Scoring() {
		
	}

	public Scoring(int firstColloquium, int secondColloquium, int finalExam, int total) {
		super();
		this.firstColloquium = firstColloquium;
		this.secondColloquium = secondColloquium;
		this.finalExam = finalExam;
		this.total = total;
	}

	public int getFirstColloquium() {
		return firstColloquium;
	}

	public void setFirstColloquium(int firstColloquium) {
		this.firstColloquium = firstColloquium;
	}

	public int getSecondColloquium() {
		return secondColloquium;
	}

	public void setSecondColloquium(int secondColloquium) {
		this.secondColloquium = secondColloquium;
	}

	public int getFinalExam() {
		return finalExam;
	}

	public void setFinalExam(int finalExam) {
		this.finalExam = finalExam;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}	
	
}
