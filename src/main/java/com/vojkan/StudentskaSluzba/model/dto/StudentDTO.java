package com.vojkan.StudentskaSluzba.model.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vojkan.StudentskaSluzba.validation.ValidProgram;

public class StudentDTO {

	private Long id;	
	
	private String username;

	@NotEmpty(message = "First name is required!")
	@Size(min = 1, max = 20, message = "First name must be minimum one character!")
	private String firstName;

	@NotEmpty(message = "Last name is required!")
	@Size(min = 1, max = 20, message = "Last name must be minimum one character!")
	private String lastName;

	@NotNull(message = "Birth date is required!")
	@Past(message = "Invalid birth date!")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy.", timezone = "Europe/Belgrade")
	private Date birth;
	
	private String address;

	@NotEmpty(message = "Card number is required!")
	@Size(min = 4, max = 4, message = "Card number must be four character!")
	private String cardNumber;

	@ValidProgram
	private ProgramDTO program;

	@NotNull(message = "Year is required!")
	@Min(value = 1, message = "Invalid year!")
	@Max(value = 4,  message = "Invalid year!")
	@Digits(fraction = 0, integer = 1,  message = "Invalid year!")
	private int year;
	
	@NotEmpty(message = "Email is required!")
	@Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Email format: user@domain.com")
	@Size(max = 50, message = "Email is too big!")
	private String email;
	
	private List<String> roles = new ArrayList<String>();
	
	@JsonIgnore
	private List<ExamDTO> exams = new ArrayList<ExamDTO>();
	
	public StudentDTO() {
		
	}

	public StudentDTO(Long id, String username, String firstName, String lastName, Date birth, String address, String cardNumber, ProgramDTO program,
			int year, String email) {
		super();
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birth = birth;
		this.address = address;
		this.cardNumber = cardNumber;
		this.program = program;
		this.year = year;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public ProgramDTO getProgram() {
		return program;
	}

	public void setProgram(ProgramDTO program) {
		this.program = program;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ExamDTO> getExams() {
		return exams;
	}

	public void setExams(List<ExamDTO> exams) {
		this.exams = exams;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
}
