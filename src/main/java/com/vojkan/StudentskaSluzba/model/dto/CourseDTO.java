package com.vojkan.StudentskaSluzba.model.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vojkan.StudentskaSluzba.validation.ValidProfessor;
import com.vojkan.StudentskaSluzba.validation.ValidProgram;

public class CourseDTO {

	private Long id;
	
	@NotEmpty(message = "Code is required!")
	private String code;

	@NotEmpty(message = "Name is required!")
	@Size(min = 3, max = 50, message = "Course name must be three or more characters!")
	private String name;	

	@NotNull(message = "Semester is required!")
	@Min(value = 1, message = "Invalid semester!")
	@Max(value = 8, message = "Invalid semester!")
	@Digits(fraction = 0, integer = 1)
	private int semester;
	
	@NotNull(message = "Year is required!")
	@Min(value = 1, message = "Invalid year!")
	@Max(value = 4,  message = "Invalid year!")
	@Digits(fraction = 0, integer = 1,  message = "Invalid year!")
	private int year;

	@NotNull(message = "Espb is required!")
	@Min(value = 1, message = "Invalid espb!")
	@Max(value = 10, message = "Invalid espb!")
	@Digits(fraction = 0, integer = 1, message = "Invalid espb! Must be between 1 and 10!")
	private int espb;
	
	private boolean registered;
	
	@ValidProgram
	private ProgramDTO program;
	
	@ValidProfessor
	private ProfessorDTO professor;
	
	@JsonIgnore
	private List<ExamDTO> exams = new ArrayList<ExamDTO>();
	
	public CourseDTO() {
		
	}

	public CourseDTO(Long id, String code, String name, int semester, int year, int espb, boolean registered, ProgramDTO program, ProfessorDTO professor) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.semester = semester;
		this.year = year;
		this.espb = espb;
		this.registered = registered;
		this.program = program;
		this.professor = professor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getEspb() {
		return espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public ProgramDTO getProgram() {
		return program;
	}

	public void setProgram(ProgramDTO program) {
		this.program = program;
	}

	public ProfessorDTO getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorDTO professor) {
		this.professor = professor;
	}

	public List<ExamDTO> getExams() {
		return exams;
	}

	public void setExams(List<ExamDTO> exams) {
		this.exams = exams;
	}
		
}
