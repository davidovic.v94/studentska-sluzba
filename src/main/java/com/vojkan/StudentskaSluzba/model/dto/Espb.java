package com.vojkan.StudentskaSluzba.model.dto;

public class Espb {

	private String espb;
	
	public Espb() {
		
	}
	
	public Espb(String espb) {
		this.espb = espb;
	}

	public String getEspb() {
		return espb;
	}

	public void setEspb(String espb) {
		this.espb = espb;
	}	
	
}
