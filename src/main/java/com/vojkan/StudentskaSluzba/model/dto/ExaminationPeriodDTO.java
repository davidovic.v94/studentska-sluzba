package com.vojkan.StudentskaSluzba.model.dto;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class ExaminationPeriodDTO {
	
	private Long id;
	
	private String name;
	
	@NotNull(message = "Date is required!")
	@Future(message = "Date must be in format: dd.MM.yyyy. (future!)")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy.", timezone = "Europe/Belgrade")
	private Date registrationStart;
	
	@NotNull(message = "Date is required!")
	@Future(message = "Date must be in format: dd.MM.yyyy. (future!)")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy.", timezone = "Europe/Belgrade")
	private Date registrationEnd;
	
	@NotNull(message = "Date is required!")
	@Future(message = "Date must be in format: dd.MM.yyyy. (future!)")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy.", timezone = "Europe/Belgrade")
	private Date start;
	
	@NotNull(message = "Date is required!")
	@Future(message = "Date must be in format: dd.MM.yyyy. (future!)")
	@JsonFormat(shape = Shape.STRING, pattern = "dd.MM.yyyy.", timezone = "Europe/Belgrade")
	private Date end;

	public ExaminationPeriodDTO() {
		
	}

	public ExaminationPeriodDTO(Long id, String name, Date registrationStart, Date registrationEnd, Date start, Date end) {
		super();
		this.id = id;
		this.name = name;
		this.registrationStart = registrationStart;
		this.registrationEnd = registrationEnd;
		this.start = start;
		this.end = end;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegistrationStart() {
		return registrationStart;
	}

	public void setRegistrationStart(Date registrationStart) {
		this.registrationStart = registrationStart;
	}

	public Date getRegistrationEnd() {
		return registrationEnd;
	}

	public void setRegistrationEnd(Date registrationEnd) {
		this.registrationEnd = registrationEnd;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}	

}
