package com.vojkan.StudentskaSluzba.model.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class NotificationDTO {

	private Long id;

	@NotEmpty(message = "Title is required!")
	@Size(min = 3, max = 50, message = "Notification title must be three or more characters!")
	private String title;

	@NotEmpty(message = "Text is required!")
	private String text;

	public NotificationDTO() {
		
	}
	
	public NotificationDTO(Long id, String title, String text) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}	
	
}
