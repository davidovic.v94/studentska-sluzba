package com.vojkan.StudentskaSluzba.model.dto;

public class Gpa {

	private String gpa;
	
	public Gpa() {
		
	}
	
	public Gpa(String gpa) {
		this.gpa = gpa;
	}

	public String getGpa() {
		return gpa;
	}

	public void setGpa(String gpa) {
		this.gpa = gpa;
	}	
	
}
