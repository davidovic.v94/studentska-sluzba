package com.vojkan.StudentskaSluzba.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.model.entity.Program;

@Repository
public interface CourseRepositoryInterface extends JpaRepository<Course, Long> {

	Course findByCode(String code);
	
	Course findByName(String name);
	
	Page<Course> findBySemester(int semester, Pageable page);
	
	Page<Course> findByEspb(int espb, Pageable page);

	Page<Course> findByProgram(Program program, Pageable page);
	
	List<Course> findByProgram(Program program);
		
	@Query("select c from Course c inner join Program p on c.program.id = p.id " + 
			"		where p.id = :programId and c.year = :year and c.name " + 
			"			not in (select c.name from Course c " + 
			"				inner join Exam e on e.course.id = c.id " + 
			"					where c.program.id = :programId and c.year = :year " +
			"						and e.student.id = :studentId and e.passed = true) " +
			"							and c.registered = false")
	Page<Course> findAvailableCoursesForRegistration(@Param("programId") Long programId, @Param("year") int year, 
			@Param("studentId") Long studentId, Pageable page);
	
	@Query("select c from Course c inner join Program p on c.program.id = p.id " + 
			"		where p.id = :programId and c.year = :year and c.name " + 
			"			not in (select c.name from Course c " + 
			"				inner join Exam e on e.course.id = c.id " + 
			"					where c.program.id = :programId and c.year = :year " +
			"						and e.student.id = :studentId and e.passed = true) " +
			"							and c.registered = true")
	Page<Course> findRegistratedCourses(@Param("programId") Long programId, @Param("year") int year, 
			@Param("studentId") Long studentId, Pageable page);
	
	List<Course> findByYear(int year);
	
	Page<Course> findByProfessor(Professor professor, Pageable page);
	
}
