package com.vojkan.StudentskaSluzba.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.Exam;

@Repository
public interface ExamRepositoryInterface extends JpaRepository<Exam, Long>{

	List<Exam> findByStudentId(Long id);
	Page<Exam> findByStudentId(Long id, Pageable page);
	
	List<Exam> findByCourseId(Long id);
	Page<Exam> findByCourseId(Long id, Pageable page);
		
	Page<Exam> findByGrade(int grade, Pageable page);

	@Query("select e from Exam e where e.student.id = :id and e.passed = true")
	Page<Exam> findPassedExamsByStudent(@Param("id") Long id, Pageable page);	
	
	@Query("SELECT avg(e.grade) from Exam e where e.student.id = :id and e.passed = true")
	double findStudentAvgGrade(@Param("id") Long id);
	
	@Query("SELECT sum(e.course.espb) from Exam e where e.student.id = :id and e.passed = true")
	int findStudentEspb(@Param("id") Long id);
	
}
