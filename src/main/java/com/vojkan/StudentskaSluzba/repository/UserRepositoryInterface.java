package com.vojkan.StudentskaSluzba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.security.User;

@Repository
public interface UserRepositoryInterface extends JpaRepository<User, Long> {

	User findByUsername(String username);
	
}
