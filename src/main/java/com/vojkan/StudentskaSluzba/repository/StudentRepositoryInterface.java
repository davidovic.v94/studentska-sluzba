package com.vojkan.StudentskaSluzba.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.Program;
import com.vojkan.StudentskaSluzba.model.entity.Student;

@Repository
public interface StudentRepositoryInterface extends JpaRepository<Student, Long>{
	
	Student findByUsername(String username);
	
	Page<Student> findByFirstName(String firstName, Pageable page);
	
	Page<Student> findByLastName(String lastName, Pageable page);
	
	Student findByCardNumber(String cardNumber);
	
	Page<Student> findByProgram(Program program, Pageable page);
	
	Page<Student> findByYear(int year, Pageable page);
	
}
