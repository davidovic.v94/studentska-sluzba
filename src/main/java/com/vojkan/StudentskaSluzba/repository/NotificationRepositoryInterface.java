package com.vojkan.StudentskaSluzba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.Notification;

@Repository
public interface NotificationRepositoryInterface extends JpaRepository<Notification, Long> {

	Notification findByTitle(String title);
	
}
