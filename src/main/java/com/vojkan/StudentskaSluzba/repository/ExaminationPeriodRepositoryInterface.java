package com.vojkan.StudentskaSluzba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.ExaminationPeriod;

@Repository
public interface ExaminationPeriodRepositoryInterface extends JpaRepository<ExaminationPeriod, Long> {
	
	ExaminationPeriod findByName(String name);

}
