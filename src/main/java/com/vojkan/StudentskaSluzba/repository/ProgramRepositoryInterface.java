package com.vojkan.StudentskaSluzba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.Program;

@Repository
public interface ProgramRepositoryInterface extends JpaRepository<Program, Long> {
	
	Program findByName(String name);
	
	Program findByTotalEspb(int espb);

}
