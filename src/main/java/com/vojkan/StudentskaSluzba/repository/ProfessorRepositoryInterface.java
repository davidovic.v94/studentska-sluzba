package com.vojkan.StudentskaSluzba.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vojkan.StudentskaSluzba.model.entity.Professor;

@Repository
public interface ProfessorRepositoryInterface extends JpaRepository<Professor, Long> {
	
	Professor findByUsername(String username);
	
	Page<Professor> findByFirstName(String firstName, Pageable page);
	
	Page<Professor> findByLastName(String lastName, Pageable page);	
	
}
