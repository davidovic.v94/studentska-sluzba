package com.vojkan.StudentskaSluzba.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.service.CourseService;

public class CustomCourseValidator implements ConstraintValidator<ValidCourse, CourseDTO>{

	@Autowired
	private CourseService courseService;
	private Course existing;
	
	@Override
	public boolean isValid(CourseDTO course, ConstraintValidatorContext context) {

		boolean valid = false;
		
		if(course.getId() != null) {
			existing = courseService.findById(course.getId());
			
			context.buildConstraintViolationWithTemplate(
					String.format("Course with id %d does not exist!", course.getId()))
					.addConstraintViolation();
			
			if(existing != null) {
				valid = true;
			}
		}
		
		return valid;
		
	}	
	
}
