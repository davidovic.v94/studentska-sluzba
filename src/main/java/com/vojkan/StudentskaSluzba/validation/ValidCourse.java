package com.vojkan.StudentskaSluzba.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = CustomCourseValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidCourse {
 
	String message() default "Invalid course!";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
