package com.vojkan.StudentskaSluzba.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.model.entity.Exam;
import com.vojkan.StudentskaSluzba.service.ExamService;

public class CustomExamValidator implements ConstraintValidator<ValidExam, ExamDTO>{

	@Autowired
	private ExamService examService;
	
	@Override
	public boolean isValid(ExamDTO exam, ConstraintValidatorContext context) {

		boolean valid = true;
		
		if((exam.getStudent().getId() != null) && (exam.getCourse().getId() != null)) {
			for(Exam e : examService.findAll()) {
				if((e.getStudent().getId().equals(exam.getStudent().getId())) && 
						(e.getCourse().getId().equals(exam.getCourse().getId())) && 
						(e.getGrade() > 5)) {
					valid = false;
					context.buildConstraintViolationWithTemplate("Student already passed this exam!")
								.addConstraintViolation();
				}
			}	
		}
		
		return valid;
		
	}	
	
}
