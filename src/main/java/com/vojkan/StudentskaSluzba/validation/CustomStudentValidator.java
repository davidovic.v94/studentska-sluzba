package com.vojkan.StudentskaSluzba.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.service.StudentService;

public class CustomStudentValidator implements ConstraintValidator<ValidStudent, StudentDTO> {

	@Autowired
	private StudentService studentService;
	private Student existing;
	
	@Override
	public boolean isValid(StudentDTO student, ConstraintValidatorContext context) {
		
		boolean valid = false;
		
		if (student.getId() != null) {
			existing = studentService.findById(student.getId());

			context.buildConstraintViolationWithTemplate(String
					.format("Student with id %d does not exist!", student.getId()))
				   	.addConstraintViolation();
			
			if(existing != null) {
				valid = true;
			}			
		}
		
		return valid;
		
	}

}
