package com.vojkan.StudentskaSluzba.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = CustomStudentValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidStudent {

	String message() default "Invalid student!";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
