package com.vojkan.StudentskaSluzba.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.vojkan.StudentskaSluzba.model.dto.ProgramDTO;
import com.vojkan.StudentskaSluzba.model.entity.Program;
import com.vojkan.StudentskaSluzba.service.ProgramService;

public class CustomProgramValidator implements ConstraintValidator<ValidProgram, ProgramDTO> {

	@Autowired
	private ProgramService programService;
	private Program existing;
	
	@Override
	public boolean isValid(ProgramDTO program, ConstraintValidatorContext context) {
		
		boolean valid = false;
		
		if(program.getId() != null) {
			existing = programService.findById(program.getId());
			
			context.buildConstraintViolationWithTemplate(
					String.format("Program with id %d does not exist!", program.getId()))
					.addConstraintViolation();
			
			if(existing != null) {
				valid = true;
			}
			
		}
		
		return valid;
		
	}

}
