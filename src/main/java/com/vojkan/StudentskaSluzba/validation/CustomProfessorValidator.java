package com.vojkan.StudentskaSluzba.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.vojkan.StudentskaSluzba.model.dto.ProfessorDTO;
import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.service.ProfessorService;

public class CustomProfessorValidator implements ConstraintValidator<ValidProfessor, ProfessorDTO> {

	@Autowired
	private ProfessorService professorService;
	private Professor existing;
	
	@Override
	public boolean isValid(ProfessorDTO professor, ConstraintValidatorContext context) {
		
		boolean valid = false;
		
		if(professor.getId() != null) {
			existing = professorService.findById(professor.getId());
			
			context.buildConstraintViolationWithTemplate(
					String.format("Professor with id %d does not exist!", professor.getId()))
					.addConstraintViolation();
			
			if(existing != null) {
				valid = true;
			}
		}
		
		return valid;
		
	}

}
