package com.vojkan.StudentskaSluzba.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = CustomProgramValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidProgram {
	
	String message() default "Invalid program!";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};

}
