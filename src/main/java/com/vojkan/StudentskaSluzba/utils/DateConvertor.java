package com.vojkan.StudentskaSluzba.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DateConvertor {
	
	private Date d;
	private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy.");
	private String strDate;

	public String dateToString(Date date) {
		strDate = formatter.format(date);		
		return strDate;
	}
	
	public Date stringToDate(String date) throws ParseException {
		d = formatter.parse(date);		
		return d;
	}
	
}
