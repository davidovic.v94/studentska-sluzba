package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;
import com.vojkan.StudentskaSluzba.model.entity.Student;

@Component
public class StudentMapper {

	private ModelMapper modelMapper = new ModelMapper();
	
	public StudentDTO converToDTO(Student student) {
		StudentDTO studentDTO = new StudentDTO();
		modelMapper.map(student, studentDTO);
		return studentDTO;
	}
	
	public Student converToEntity(StudentDTO studentDTO) {
		Student student = new Student();
		modelMapper.map(studentDTO, student);
		return student;
	}
	
}
