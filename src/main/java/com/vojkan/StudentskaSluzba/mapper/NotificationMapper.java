package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.NotificationDTO;
import com.vojkan.StudentskaSluzba.model.entity.Notification;

@Component
public class NotificationMapper {

	private ModelMapper modelMapper = new ModelMapper();
	
	public NotificationDTO convertToDTO(Notification notification) {
		NotificationDTO notificationDTO = new NotificationDTO();
		modelMapper.map(notification, notificationDTO);
		return notificationDTO;
	}
	
	public Notification convertToEntity(NotificationDTO notificationDTO) {
		Notification notification = new Notification();
		modelMapper.map(notificationDTO, notification);
		return notification;
	}
	
}
