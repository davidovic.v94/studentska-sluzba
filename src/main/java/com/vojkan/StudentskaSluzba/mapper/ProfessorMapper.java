package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.ProfessorDTO;
import com.vojkan.StudentskaSluzba.model.entity.Professor;

@Component
public class ProfessorMapper {

	private ModelMapper modelMapper = new ModelMapper();
	
	public ProfessorDTO converToDTO(Professor professor) {
		ProfessorDTO professorDTO = new ProfessorDTO();
		modelMapper.map(professor, professorDTO);
		return professorDTO;
	}
	
	public Professor converToEntity(ProfessorDTO professorDTO) {
		Professor professor = new Professor();
		modelMapper.map(professorDTO, professor);
		return professor;
	}
	
}
