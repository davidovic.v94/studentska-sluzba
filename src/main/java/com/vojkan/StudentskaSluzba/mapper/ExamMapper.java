package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.model.entity.Exam;

@Component
public class ExamMapper {

	private ModelMapper modelMapper = new ModelMapper();
	
	public ExamDTO converToDTO(Exam exam) {
		ExamDTO examDTO = new ExamDTO();
		modelMapper.map(exam, examDTO);
		return examDTO;
	}
	
	public Exam converToEntity(ExamDTO examDTO) {
		Exam exam = new Exam();
		modelMapper.map(examDTO, exam);
		return exam;
	}
	
}
