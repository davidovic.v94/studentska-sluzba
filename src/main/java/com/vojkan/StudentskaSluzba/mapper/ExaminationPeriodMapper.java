package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.ExaminationPeriodDTO;
import com.vojkan.StudentskaSluzba.model.entity.ExaminationPeriod;

@Component
public class ExaminationPeriodMapper {

private ModelMapper modelMapper = new ModelMapper();
	
	public ExaminationPeriodDTO converToDTO(ExaminationPeriod examinationPeriod) {
		ExaminationPeriodDTO examinationPeriodDTO = new ExaminationPeriodDTO();
		modelMapper.map(examinationPeriod, examinationPeriodDTO);
		return examinationPeriodDTO;
	}
	
	public ExaminationPeriod converToEntity(ExaminationPeriodDTO examinationPeriodDTO) {
		ExaminationPeriod examinationPeriod = new ExaminationPeriod();
		modelMapper.map(examinationPeriodDTO, examinationPeriod);
		return examinationPeriod;
	}
	
}
