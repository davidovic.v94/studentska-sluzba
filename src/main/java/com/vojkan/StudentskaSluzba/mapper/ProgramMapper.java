package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.ProgramDTO;
import com.vojkan.StudentskaSluzba.model.entity.Program;

@Component
public class ProgramMapper {

	private ModelMapper modelMapper = new ModelMapper();
	
	public ProgramDTO convertToDTO(Program program) {
		ProgramDTO programDTO = new ProgramDTO();
		modelMapper.map(program, programDTO);
		return programDTO;
	}
	
	public Program convertToEntity(ProgramDTO programDTO) {
		Program program = new Program();
		modelMapper.map(programDTO, program);
		return program;
	}
	
}
