package com.vojkan.StudentskaSluzba.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.entity.Course;

@Component
public class CourseMapper {

	private ModelMapper modelMapper = new ModelMapper();
	
	public CourseDTO converToDTO(Course course) {
		CourseDTO courseDTO = new CourseDTO();
		modelMapper.map(course, courseDTO);
		return courseDTO;
	}
	
	public Course converToEntity(CourseDTO courseDTO) {
		Course course = new Course();
		modelMapper.map(courseDTO, course);
		return course;
	}
	
}
