package com.vojkan.StudentskaSluzba.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.ExamControllerInteface;
import com.vojkan.StudentskaSluzba.mapper.ExamMapper;
import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.model.entity.Exam;
import com.vojkan.StudentskaSluzba.service.ExamService;
import com.vojkan.StudentskaSluzba.validation.ValidExam;

import javassist.NotFoundException;

@Validated
@RestController
public class ExamController implements ExamControllerInteface {

	private final ExamService examService;
	private final ExamMapper examMapper;
	
	@Autowired
	public ExamController(ExamService examService, ExamMapper examMapper) {
		this.examService = examService;
		this.examMapper = examMapper;
	}
	
	@Override
	public ResponseEntity<List<ExamDTO>> getExamsPage(Pageable page) {
		Page<Exam> exams = examService.findAll(page);
		List<ExamDTO> examDTOs = new ArrayList<>();
		examDTOs = exams.stream()
				.map(examMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(examDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExamDTO> getExamById(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException {
		Exam exam = examService.findById(id);
		if (exam == null) {
			throw new NotFoundException(String.format("Exam with id %d is not found!", id));
		}
		ExamDTO examDTO = new ExamDTO();
		examDTO = examMapper.converToDTO(exam);
		return new ResponseEntity<>(examDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<ExamDTO>> getExamsByDate(@RequestParam String date, Pageable page) 
			throws ParseException {
		Page<Exam> exams = examService.findByDate(date, page);
		List<ExamDTO> examDTOs = new ArrayList<>();
		examDTOs = exams.stream()
				.map(examMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(examDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<ExamDTO>> getExamsByGrade(@Min(value = 6) @Max(value = 10)
			@RequestParam int grade, Pageable page) {
		Page<Exam> exams = examService.findByGrade(grade, page);
		List<ExamDTO> examDTOs = new ArrayList<>();
		examDTOs = exams.stream()
				.map(examMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(examDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExamDTO> createExam(@Valid @ValidExam @RequestBody ExamDTO examDTO) {
		Exam exam = examMapper.converToEntity(examDTO);
		Exam toCreate = new Exam();
		examService.isPassed(exam);
		toCreate = examService.save(exam);
		ExamDTO created = new ExamDTO();
		created = examMapper.converToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<ExamDTO> updateteExam(@Min(value = 1) @PathVariable Long id,
			@Valid @RequestBody ExamDTO examDTO) throws NotFoundException {
		Exam exam = examService.findById(id);
		if (exam == null) {
			throw new NotFoundException(String.format("Exam with id %d is not found!", id));
		}
		exam = examMapper.converToEntity(examDTO);
		exam.setId(id);
		Exam toUpdate = new Exam();
		toUpdate = examService.save(exam);
		ExamDTO updated = new ExamDTO();
		updated = examMapper.converToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExamDTO> deleteExam(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException {
		Exam exam = examService.findById(id);
		if (exam == null) {
			throw new NotFoundException(String.format("Exam with id %d is not found!", id));
		}
		ExamDTO deleted = new ExamDTO();
		deleted = examMapper.converToDTO(exam);
		examService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

}
