package com.vojkan.StudentskaSluzba.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.ProgramControllerInterface;
import com.vojkan.StudentskaSluzba.mapper.CourseMapper;
import com.vojkan.StudentskaSluzba.mapper.ProgramMapper;
import com.vojkan.StudentskaSluzba.mapper.StudentMapper;
import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.ProgramDTO;
import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;
import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.model.entity.Program;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.service.CourseService;
import com.vojkan.StudentskaSluzba.service.ProgramService;
import com.vojkan.StudentskaSluzba.service.StudentService;

import javassist.NotFoundException;

@RestController
public class ProgramController implements ProgramControllerInterface {
	
	private final ProgramService programService;
	private final ProgramMapper programMapper;
	private final CourseService courseService;
	private final CourseMapper courseMapper;
	private final StudentService studentService;
	private final StudentMapper studentMapper;
	
	@Autowired
	public ProgramController(ProgramService programService, ProgramMapper programMapper, CourseService courseService,
			CourseMapper courseMapper, StudentService studentService, StudentMapper studentMapper) {
		this.programService = programService;
		this.programMapper = programMapper;
		this.courseService = courseService;
		this.courseMapper = courseMapper;
		this.studentService = studentService;
		this.studentMapper = studentMapper;
	}

	@Override
	public ResponseEntity<List<ProgramDTO>> getProgramsPage(Pageable page) {
		Page<Program> programs = programService.findAll(page);
		List<ProgramDTO> programDTOs = new ArrayList<ProgramDTO>();
		programDTOs = programs.stream()
				.map(programMapper::convertToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(programDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProgramDTO> getProgramById(@Min(1) Long id) throws NotFoundException {
		Program program = programService.findById(id);
		if (program ==null) {
			throw new NotFoundException(String.format("Program with id %d is not found!", id));
		}
		ProgramDTO programDTO = new ProgramDTO();
		programDTO = programMapper.convertToDTO(program);
		return new ResponseEntity<>(programDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProgramDTO> getProgramByName(String name) throws NotFoundException {
		Program program = programService.findByName(name);
		if (program ==null) {
			throw new NotFoundException(String.format("Program with name %s is not found!", name));
		}
		ProgramDTO programDTO = new ProgramDTO();
		programDTO = programMapper.convertToDTO(program);
		return new ResponseEntity<>(programDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<CourseDTO>> getCoursesByProgram(@Min(1) Long id, Pageable page)
			throws NotFoundException {
		Program program = programService.findById(id);
		if (program == null) {
			throw new NotFoundException(String.format("Program with id %d is not found!", id));
		}
		List<CourseDTO> courseDTOs = new ArrayList<>();
		if (!program.getCourses().isEmpty()) {
			Page<Course> courses = courseService.findByProgram(program, page);
			courseDTOs = courses.stream()
					.map(courseMapper::converToDTO)
					.collect(Collectors.toList());			
		}
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<StudentDTO>> getStudentsByProgram(@Min(1) Long id, Pageable page)
			throws NotFoundException {
		Program program = programService.findById(id);
		if (program == null) {
			throw new NotFoundException(String.format("Program with id %d is not found!", id));
		}
		List<StudentDTO> studentDTOs = new ArrayList<>();
		if (!program.getStudents().isEmpty()) {
			Page<Student> students = studentService.findByProgram(program, page);
			studentDTOs = students.stream()
					.map(studentMapper::converToDTO)
					.collect(Collectors.toList());			
		}
		return new ResponseEntity<>(studentDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProgramDTO> createProgram(@Valid ProgramDTO programDTO)
			throws SQLIntegrityConstraintViolationException {
		Program program = programMapper.convertToEntity(programDTO);
		if(!programService.isValidName(program.getName())) {
			throw new SQLIntegrityConstraintViolationException(
					String.format("Course with name %s already exist!", program.getName()));
		}
		Program toCreate = new Program();
		toCreate = programService.save(program);
		ProgramDTO created = new ProgramDTO();
		created = programMapper.convertToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<ProgramDTO> updateProgram(@Min(1) Long id, @Valid ProgramDTO programDTO)
			throws NotFoundException {
		Program program = programService.findById(id);
		if (program == null) {
			throw new NotFoundException(String.format("Program with id %d is not found!", id));
		}
		program = programMapper.convertToEntity(programDTO);
		program.setId(id);
		Program toUpdate = new Program();
		toUpdate = programService.save(program);
		ProgramDTO updated = new ProgramDTO();
		updated = programMapper.convertToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProgramDTO> deleteProgram(@Min(1) Long id) throws NotFoundException {
		Program program = programService.findById(id);
		if (program == null) {
			throw new NotFoundException(String.format("Program with id %d is not found!", id));
		}
		ProgramDTO deleted = new ProgramDTO();
		deleted = programMapper.convertToDTO(program);
		programService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

}
