package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.validation.ValidExam;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/exams")
public interface ExamControllerInteface {

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ExamDTO>> getExamsPage(Pageable page);

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExamDTO> getExamById(@Min(value = 1) @PathVariable Long id) throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "date")
	ResponseEntity<List<ExamDTO>> getExamsByDate(@RequestParam String date, Pageable page) 
			throws ParseException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "grade")
	ResponseEntity<List<ExamDTO>> getExamsByGrade(@Min(value = 6) @Max(value = 10) 
		@RequestParam int grade, Pageable page);
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExamDTO> createExam(@Valid @ValidExam @RequestBody ExamDTO examDTO);
	
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExamDTO> updateteExam(@Min(value = 1) @PathVariable Long id,
			@Valid @RequestBody ExamDTO examDTO) throws NotFoundException;
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExamDTO> deleteExam(@Min(value = 1) @PathVariable Long id) throws NotFoundException;
	
}
