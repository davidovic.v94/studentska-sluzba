package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.Espb;
import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.model.dto.Gpa;
import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/students")
public interface StudentControllerInterface {

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<StudentDTO>> getStudentsPage(Pageable page);

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<StudentDTO> getStudentById(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "firstName")
	ResponseEntity<List<StudentDTO>> getStudentsByFirstName(@RequestParam String firstName, Pageable page);

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "lastName")
	ResponseEntity<List<StudentDTO>> getStudentsByLastName(@RequestParam String lastName, Pageable page);
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "cardNumber")
	ResponseEntity<StudentDTO> getStudentByCardNumber(@RequestParam String cardNumber) 
			throws NotFoundException;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "year")
	ResponseEntity<List<StudentDTO>> getStudentsByYear(@Min(value = 1) @Max(value = 10) 
			@RequestParam int year, Pageable page);
	
	@GetMapping(value = "/{id}/exams", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ExamDTO>> getExamsByStudentPage(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException;
	
	@GetMapping(value = "/{id}/passed-exams", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ExamDTO>> getPassedExamsByStudentPage(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException;
	
	@GetMapping(value = "/{id}/exams/for-registration", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<CourseDTO>> getExamsForRegistration(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException;
	
	@GetMapping(value = "/{id}/exams/registered", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<CourseDTO>> getRegisteredExams(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException;
	
	@GetMapping(value = "/{id}/exams/avg", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Gpa> getStudentAvgGrade(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;
	
	@GetMapping(value = "/{id}/exams/espb", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Espb> getStudentEspb(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<StudentDTO> createStudent(@Valid @RequestBody StudentDTO studentDTO)
			throws SQLIntegrityConstraintViolationException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<StudentDTO> updateStudent(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody StudentDTO studentDTO) throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<StudentDTO> deleteStudent(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;
	
}
