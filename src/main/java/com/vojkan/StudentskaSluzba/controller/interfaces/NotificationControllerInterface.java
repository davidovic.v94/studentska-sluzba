package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vojkan.StudentskaSluzba.model.dto.NotificationDTO;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/notifications")
public interface NotificationControllerInterface {
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<NotificationDTO>> getNotificationsPage(Pageable page);
		
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<NotificationDTO> getNotificationById(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;
	
	@GetMapping(params = "title", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<NotificationDTO> getNotificationByTitle(@RequestParam String title) 
			throws NotFoundException;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<NotificationDTO> createNotification(@Valid @RequestBody NotificationDTO notificationDTO);
	
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<NotificationDTO> updateNotification(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody NotificationDTO notificationDTO) throws NotFoundException;
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<NotificationDTO> deleteNotification(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;

}
