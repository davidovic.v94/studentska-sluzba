package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/courses")
public interface CourseControllerInterface {

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<CourseDTO>> getCoursesPage(Pageable page);

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<CourseDTO> getCourseById(@Min(value = 1) @PathVariable Long id)
			throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "code")
	ResponseEntity<CourseDTO> getCourseByCode(@RequestParam String code)
			throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "name")
	ResponseEntity<CourseDTO> getCourseByName(@RequestParam String name)
			throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "semester")
	ResponseEntity<List<CourseDTO>> getCoursesBySemester(@Min(value = 1) @Max(value = 8) 
			@RequestParam int semester, Pageable page);

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "espb")
	ResponseEntity<List<CourseDTO>> getCoursesByEspb(@Min(value = 1) @Max(value = 10) 
			@RequestParam int espb, Pageable page);
	
	@GetMapping(value = "/{id}/exams", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ExamDTO>> getExamsByCoursePage(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException;
	
	@PutMapping(value = "/{id}/register", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<CourseDTO> registerCourse(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody CourseDTO courseDTO) throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<CourseDTO> createCourse(@Valid @RequestBody CourseDTO courseDTO)
			throws SQLIntegrityConstraintViolationException;
	
//	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<CourseDTO> updateCourse(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody CourseDTO courseDTO) throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<CourseDTO> deleteCourse(@Min(value = 1) @PathVariable Long id)
			throws NotFoundException;

}
