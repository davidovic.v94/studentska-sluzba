package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.ProfessorDTO;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/professors")
public interface ProfessorControllerInterface {

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ProfessorDTO>> getProfessorsPage(Pageable page);

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProfessorDTO> getProfessorById(@Min(value = 1) @PathVariable Long id)
			throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "firstName")
	ResponseEntity<List<ProfessorDTO>> getProfessorsByFirstName(@RequestParam String firstName, 
			Pageable page);

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "lastName")
	ResponseEntity<List<ProfessorDTO>> getProfessorsByLastName(@RequestParam String lastName, 
			Pageable page);

	@GetMapping(value = "/{id}/courses", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<CourseDTO>> getCoursesByProfessor(@Min(value = 1) @PathVariable Long id,
			Pageable page) throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProfessorDTO> createProfessor(@Valid @RequestBody ProfessorDTO professorDTO);

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProfessorDTO> updateProfessor(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody ProfessorDTO professorDTO) throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProfessorDTO> deleteProfessor(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException;
	
}
