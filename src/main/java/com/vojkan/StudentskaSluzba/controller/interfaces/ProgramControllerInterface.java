package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.ProgramDTO;
import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/programs")
public interface ProgramControllerInterface {
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ProgramDTO>> getProgramsPage(Pageable page);
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProgramDTO> getProgramById(@Min(value = 1) @PathVariable Long id) throws NotFoundException;
	
	@GetMapping(params = "name", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProgramDTO> getProgramByName(@RequestParam String name) 
			throws NotFoundException;
	
	@GetMapping(value = "/{id}/courses", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<CourseDTO>> getCoursesByProgram(@Min(value = 1) @PathVariable Long id, Pageable page) 
			throws NotFoundException;
	
	@GetMapping(value = "/{id}/students", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<StudentDTO>> getStudentsByProgram(@Min(value = 1) @PathVariable Long id, Pageable page) 
			throws NotFoundException;
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProgramDTO> createProgram(@Valid @RequestBody ProgramDTO programDTO) throws SQLIntegrityConstraintViolationException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProgramDTO> updateProgram(@Min(value = 1) @PathVariable Long id, @Valid @RequestBody ProgramDTO programDTO) 
			throws NotFoundException;
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ProgramDTO> deleteProgram(@Min(value = 1) @PathVariable Long id) throws NotFoundException;

}
