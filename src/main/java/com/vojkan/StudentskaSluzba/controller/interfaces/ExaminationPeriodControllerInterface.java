package com.vojkan.StudentskaSluzba.controller.interfaces;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vojkan.StudentskaSluzba.model.dto.ExaminationPeriodDTO;

import javassist.NotFoundException;

@Validated
@RequestMapping(value = "api/examination-period")
public interface ExaminationPeriodControllerInterface {
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<ExaminationPeriodDTO>> getExaminationPeriodPage(Pageable page);
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExaminationPeriodDTO> getExaminationPeriodById(@Min(value = 1) @PathVariable Long id) throws NotFoundException;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = "name")
	ResponseEntity<ExaminationPeriodDTO> getExaminationPeriodByName(@RequestParam String name) throws NotFoundException;

	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExaminationPeriodDTO> create(@Valid @RequestBody ExaminationPeriodDTO examinationPeriodDTO);
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExaminationPeriodDTO> update(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody ExaminationPeriodDTO examinationPeriodDTO) throws NotFoundException;
	
	@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ExaminationPeriodDTO> delete(@Min(value = 1) @PathVariable Long id) throws NotFoundException;

}
