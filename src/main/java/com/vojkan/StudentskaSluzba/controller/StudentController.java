package com.vojkan.StudentskaSluzba.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.StudentControllerInterface;
import com.vojkan.StudentskaSluzba.mapper.CourseMapper;
import com.vojkan.StudentskaSluzba.mapper.ExamMapper;
import com.vojkan.StudentskaSluzba.mapper.StudentMapper;
import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.Espb;
import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.model.dto.Gpa;
import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;
import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.model.entity.Exam;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.service.CourseService;
import com.vojkan.StudentskaSluzba.service.ExamService;
import com.vojkan.StudentskaSluzba.service.ExaminationPeriodService;
import com.vojkan.StudentskaSluzba.service.StudentService;

import javassist.NotFoundException;

@RestController
public class StudentController implements StudentControllerInterface {

	private final StudentService studentService;
	private final ExamService examService;
	private final StudentMapper studentMapper;
	private final ExamMapper examMapper;
	private final CourseService courseService;
	private final CourseMapper courseMapper;
	private final ExaminationPeriodService examinationPeriodService;
	
	@Autowired
	public StudentController(StudentService studentService, StudentMapper studentMapper, ExamService examService, 
			ExamMapper examMapper, CourseService courseService, CourseMapper courseMapper,  ExaminationPeriodService examinationPeriodService) {
		this.studentService = studentService;
		this.studentMapper = studentMapper;
		this.examService = examService;
		this.examMapper = examMapper;
		this.courseService = courseService;
		this.courseMapper = courseMapper;
		this.examinationPeriodService = examinationPeriodService;
	}
	
	@Override
	public ResponseEntity<List<StudentDTO>> getStudentsPage(Pageable page) {
		Page<Student> students = studentService.findAll(page);
		List<StudentDTO> studentDTOs = new ArrayList<>();
		studentDTOs = students.stream()
				.map(studentMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(studentDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<StudentDTO> getStudentById(@Min(value = 1) @PathVariable Long id) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		StudentDTO studentDTO = new StudentDTO();
		studentDTO = studentMapper.converToDTO(student);
		return new ResponseEntity<>(studentDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<StudentDTO>> getStudentsByFirstName(@RequestParam String firstName, Pageable page) {
		Page<Student> students = studentService.findByFirstName(firstName, page);
		List<StudentDTO> studentDTOs = new ArrayList<>();
		studentDTOs = students.stream()
				.map(studentMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(studentDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<StudentDTO>> getStudentsByLastName(@RequestParam String lastName, Pageable page) {
		Page<Student> students = studentService.findByLastName(lastName, page);
		List<StudentDTO> studentDTOs = new ArrayList<>();
		studentDTOs = students.stream()
				.map(studentMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(studentDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<StudentDTO> getStudentByCardNumber(@RequestParam String cardNumber) 
			throws NotFoundException {
		Student student = studentService.findByCardNumber(cardNumber);
		if (student == null) {
			throw new NotFoundException(String.format("Student with card number %s is not found!", 
					cardNumber));
		}
		StudentDTO studentDTO = new StudentDTO();
		studentDTO = studentMapper.converToDTO(student);
		return new ResponseEntity<>(studentDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<StudentDTO>> getStudentsByYear(@Min(value = 1) @Max(value = 10) 
			@RequestParam int year, Pageable page) {
		Page<Student> students = studentService.findByYear(year, page);
		List<StudentDTO> studentDTOs = new ArrayList<>();
		studentDTOs = students.stream()
				.map(studentMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(studentDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<ExamDTO>> getExamsByStudentPage(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		List<ExamDTO> examDTOs = new ArrayList<>();
		if (!student.getExams().isEmpty()) {
			Page<Exam> exams = examService.findByStudentId(id, page);
			examDTOs = exams.stream()
					.map(examMapper::converToDTO)
					.collect(Collectors.toList());
		}
		return new ResponseEntity<>(examDTOs, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<List<ExamDTO>> getPassedExamsByStudentPage(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		List<ExamDTO> examDTOs = new ArrayList<>();
		if (!student.getExams().isEmpty()) {
			Page<Exam> exams = examService.findPassedExamsByStudent(id, page);
			examDTOs = exams.stream()
					.map(examMapper::converToDTO)
					.collect(Collectors.toList());
		}
		return new ResponseEntity<>(examDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Gpa> getStudentAvgGrade(@Min(value = 1) @PathVariable Long id) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		double avgGrade = examService.findStudentAvgGrade(id);
		Gpa gpa = new Gpa(String.format("%.2f", avgGrade));
		return new ResponseEntity<>(gpa, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<Espb> getStudentEspb(@Min(value = 1) @PathVariable Long id) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		int espbSum = examService.findStudentEspb(id);
		Espb espb = new Espb(String.format("%d", espbSum));
		return new ResponseEntity<>(espb, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<CourseDTO>> getExamsForRegistration(@Min(1) Long id, Pageable page)
			throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		Page<Course> examsForRegistration = courseService.findAvailableCoursesForRegistration(student, page);
		List<CourseDTO> courseDTOs = new ArrayList<>();
		if (examinationPeriodService.isRegistrationPeriod()) {
			courseDTOs = examsForRegistration.stream()
					.map(courseMapper::converToDTO)
					.collect(Collectors.toList());
		}	
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<CourseDTO>> getRegisteredExams(@Min(1) Long id, Pageable page) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		}
		Page<Course> registeredExams = courseService.findRegistratedCourses(student, page);
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs = registeredExams.stream()
				.map(courseMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<StudentDTO> createStudent(@Valid @RequestBody StudentDTO studentDTO) 
			throws SQLIntegrityConstraintViolationException {
		Student student = studentMapper.converToEntity(studentDTO);
		if(!studentService.isValidCardNumber(student.getCardNumber())) {
			throw new SQLIntegrityConstraintViolationException(
					String.format("Student with card number %s already exist!", student.getCardNumber()));
		}
		Student toCreate = new Student();
		toCreate = studentService.save(student);
		StudentDTO created = new StudentDTO();
		created = studentMapper.converToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<StudentDTO> updateStudent(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody StudentDTO studentDTO) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		} 
		student = studentMapper.converToEntity(studentDTO);
		student.setId(id);
		Student toUpdate = new Student();
		toUpdate = studentService.save(student);
		StudentDTO updated = new StudentDTO();
		updated = studentMapper.converToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<StudentDTO> deleteStudent(@Min(value = 1) @PathVariable Long id) throws NotFoundException {
		Student student = studentService.findById(id);
		if (student == null) {
			throw new NotFoundException(String.format("Student with id %d is not found!", id));
		} 
		StudentDTO deleted = new StudentDTO();
		deleted = studentMapper.converToDTO(student);
		studentService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}
	
}
