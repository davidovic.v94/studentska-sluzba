package com.vojkan.StudentskaSluzba.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.NotificationControllerInterface;
import com.vojkan.StudentskaSluzba.mapper.NotificationMapper;
import com.vojkan.StudentskaSluzba.model.dto.NotificationDTO;
import com.vojkan.StudentskaSluzba.model.entity.Notification;
import com.vojkan.StudentskaSluzba.service.NotificationService;

import javassist.NotFoundException;

@Validated
@RestController
public class NotificationController implements NotificationControllerInterface {

	private final NotificationService notificationService;
	private final NotificationMapper notificationMapper;
	
	@Autowired
	public NotificationController(NotificationService notificationService, NotificationMapper notificationMapper) {
		this.notificationService = notificationService;
		this.notificationMapper = notificationMapper;
	}

	@Override
	public ResponseEntity<List<NotificationDTO>> getNotificationsPage(Pageable page) {
		Page<Notification> notifications = notificationService.findAll(page);
		List<NotificationDTO> notificationDTOs = new ArrayList<>();
		notificationDTOs = notifications.stream()
				.map(notificationMapper::convertToDTO)
				.collect(Collectors.toList());				
		return new ResponseEntity<>(notificationDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<NotificationDTO> getNotificationById(@Min(1) Long id) throws NotFoundException {
		Notification notification = notificationService.findById(id);
		if (notification == null) {
			throw new NotFoundException(String.format("Notification with id %d is not found!", id));			
		}
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO = notificationMapper.convertToDTO(notification);
		return new ResponseEntity<>(notificationDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<NotificationDTO> getNotificationByTitle(String title) throws NotFoundException {
		Notification notification = notificationService.findByTitle(title);
		if (notification == null) {
			throw new NotFoundException(String.format("Notification with title %s is not found!", title));			
		}
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO = notificationMapper.convertToDTO(notification);
		return new ResponseEntity<>(notificationDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<NotificationDTO> createNotification(@Valid NotificationDTO notificationDTO) {
		Notification notification = notificationMapper.convertToEntity(notificationDTO);
		Notification toCreate = new Notification();
		toCreate = notificationService.save(notification);
		NotificationDTO created = new NotificationDTO();
		created = notificationMapper.convertToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<NotificationDTO> updateNotification(@Min(1) Long id, @Valid NotificationDTO notificationDTO)
			throws NotFoundException {
		Notification notification = notificationService.findById(id);
		if (notification == null) {
			throw new NotFoundException(String.format("Notification with id %d is not found!", id));				
		}
		notification = notificationMapper.convertToEntity(notificationDTO);
		notification.setId(id);
		Notification toUpdate = new Notification();
		toUpdate = notificationService.save(notification);
		NotificationDTO updated = new NotificationDTO();
		updated = notificationMapper.convertToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<NotificationDTO> deleteNotification(@Min(1) Long id) throws NotFoundException {
		Notification notification = notificationService.findById(id);
		if (notification == null) {
			throw new NotFoundException(String.format("Course with id %d is not found!", id));			
		}
		NotificationDTO deleted = new NotificationDTO();
		deleted = notificationMapper.convertToDTO(notification);
		notificationService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

}
