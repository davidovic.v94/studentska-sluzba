package com.vojkan.StudentskaSluzba.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.CourseControllerInterface;
import com.vojkan.StudentskaSluzba.mapper.CourseMapper;
import com.vojkan.StudentskaSluzba.mapper.ExamMapper;
import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.ExamDTO;
import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.model.entity.Exam;
import com.vojkan.StudentskaSluzba.service.CourseService;
import com.vojkan.StudentskaSluzba.service.ExamService;

import javassist.NotFoundException;

@Validated
@RestController
public class CourseController implements  CourseControllerInterface {

	private final CourseService courseService;
	private final ExamService examService;
	private final CourseMapper courseMapper;
	private final ExamMapper examMapper;
	
	@Autowired
	public CourseController(CourseService courseService, ExamService examService, 
			CourseMapper courseMapper, ExamMapper examMapper) {
		this.courseService = courseService;
		this.examService = examService;
		this.courseMapper = courseMapper;
		this.examMapper = examMapper;
	}
	
	@Override
	public ResponseEntity<List<CourseDTO>> getCoursesPage(Pageable page) {
		Page<Course> courses = courseService.findAll(page);
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs = courses.stream()
				.map(courseMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<CourseDTO> getCourseById(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException {
		Course course = courseService.findById(id);
		if (course == null) {
			throw new NotFoundException(String.format("Course with id %d is not found!", id));
		}
		CourseDTO courseDTO = new CourseDTO();
		courseDTO = courseMapper.converToDTO(course);
		return new ResponseEntity<>(courseDTO, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<CourseDTO> getCourseByCode(@RequestParam String code) throws NotFoundException {
		Course course = courseService.findByCode(code);
		if (course == null) {
			throw new NotFoundException(String.format("Course with code %s is not found!", code));
		}
		CourseDTO courseDTO = new CourseDTO();
		courseDTO = courseMapper.converToDTO(course);
		return new ResponseEntity<>(courseDTO, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<CourseDTO> getCourseByName(@RequestParam String name) throws NotFoundException {
		Course course = courseService.findByName(name);
		if (course == null) {
			throw new NotFoundException(String.format("Course with name %s is not found!", name));
		}
		CourseDTO courseDTO = new CourseDTO();
		courseDTO = courseMapper.converToDTO(course);
		return new ResponseEntity<>(courseDTO, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<List<CourseDTO>> getCoursesBySemester(@Min(value = 1) @Max(value = 8) 
			@RequestParam int semester, Pageable page) {
		Page<Course> courses = courseService.findBySemester(semester, page);
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs = courses.stream()
				.map(courseMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<List<CourseDTO>> getCoursesByEspb(@Min(value = 1) @Max(value = 10)
			@RequestParam int espb, Pageable page) {
		Page<Course> courses = courseService.findByEspb(espb, page);
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs = courses.stream()
				.map(courseMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<ExamDTO>> getExamsByCoursePage(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException {
		Course course = courseService.findById(id);
		if (course == null) {
			throw new NotFoundException(String.format("Course with id %d is not found!", id));
		}
		List<ExamDTO> examDTOs = new ArrayList<>();
		if (!course.getExams().isEmpty()) {
			Page<Exam> exams = examService.findByCourseId(id, page);
			examDTOs = exams.stream()
					.map(examMapper::converToDTO)
					.collect(Collectors.toList());
		}
		return new ResponseEntity<>(examDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CourseDTO> registerCourse(@Min(1) Long id, @Valid CourseDTO courseDTO)
			throws NotFoundException {
		Course course = courseService.findById(id);
		if (course == null) {
			throw new NotFoundException(String.format("Course with id %d is not found!", id));
		}
		course = courseMapper.converToEntity(courseDTO);
		course.setId(id);
		course.setRegistered(true);
		Course toRegister = new Course();
		toRegister = courseService.save(course);
		CourseDTO registered = new CourseDTO();
		registered = courseMapper.converToDTO(toRegister);
		return new ResponseEntity<>(registered, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<CourseDTO> createCourse(@Valid @RequestBody CourseDTO courseDTO) 
			throws SQLIntegrityConstraintViolationException {
		Course course = courseMapper.converToEntity(courseDTO);
		if(!courseService.isValidCode(course.getCode())) {
			throw new SQLIntegrityConstraintViolationException(
					String.format("Course with code %s already exist!", course.getCode()));
		}
		Course toCreate = new Course();
		toCreate = courseService.save(course);
		CourseDTO created = new CourseDTO();
		created = courseMapper.converToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}
	
	@Override
	public ResponseEntity<CourseDTO> updateCourse(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody CourseDTO courseDTO) throws NotFoundException {
		Course course = courseService.findById(id);
		if (course == null) {
			throw new NotFoundException(String.format("Course with id %d is not found!", id));
		}
		course = courseMapper.converToEntity(courseDTO);
		course.setId(id);
		Course toUpdate = new Course();
		toUpdate = courseService.save(course);
		CourseDTO updated = new CourseDTO();
		updated = courseMapper.converToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<CourseDTO> deleteCourse(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException {
		Course course = courseService.findById(id);
		if (course == null) {
			throw new NotFoundException(String.format("Course with id %d is not found!", id));
		}
		CourseDTO deleted = new CourseDTO();
		deleted = courseMapper.converToDTO(course);
		courseService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}
	
}
