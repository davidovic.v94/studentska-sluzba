package com.vojkan.StudentskaSluzba.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.ExaminationPeriodControllerInterface;
import com.vojkan.StudentskaSluzba.mapper.ExaminationPeriodMapper;
import com.vojkan.StudentskaSluzba.model.dto.ExaminationPeriodDTO;
import com.vojkan.StudentskaSluzba.model.entity.ExaminationPeriod;
import com.vojkan.StudentskaSluzba.service.ExaminationPeriodService;

import javassist.NotFoundException;

@Validated
@RestController
public class ExaminationPeriodController implements ExaminationPeriodControllerInterface {
	
	private final ExaminationPeriodService examinationPeriodService;
	private final ExaminationPeriodMapper examinationPeriodMapper;
	
	@Autowired	
	public ExaminationPeriodController(ExaminationPeriodService examinationPeriodService,
			ExaminationPeriodMapper examinationPeriodMapper) {
		this.examinationPeriodService = examinationPeriodService;
		this.examinationPeriodMapper = examinationPeriodMapper;
	}

	@Override
	public ResponseEntity<List<ExaminationPeriodDTO>> getExaminationPeriodPage(Pageable page) {
		Page<ExaminationPeriod> ep = examinationPeriodService.findAll(page);
		List<ExaminationPeriodDTO> epDTO = new ArrayList<>();
		epDTO = ep.stream()
				.map(examinationPeriodMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(epDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExaminationPeriodDTO> getExaminationPeriodById(@Min(1) Long id) throws NotFoundException {
		ExaminationPeriod ep = examinationPeriodService.findById(id);
		if (ep == null) {
			throw new NotFoundException(String.format("Examination period with id %d is not found!", id));
		}
		ExaminationPeriodDTO epDTO = new ExaminationPeriodDTO();
		epDTO = examinationPeriodMapper.converToDTO(ep);
		return new ResponseEntity<>(epDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExaminationPeriodDTO> getExaminationPeriodByName(String name) throws NotFoundException {
		ExaminationPeriod ep = examinationPeriodService.findByName(name);
		if (ep == null) {
			throw new NotFoundException(String.format("Examination period with name %s is not found!", name));
		}
		ExaminationPeriodDTO epDTO = new ExaminationPeriodDTO();
		epDTO = examinationPeriodMapper.converToDTO(ep);
		return new ResponseEntity<>(epDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExaminationPeriodDTO> create(@Valid ExaminationPeriodDTO examinationPeriodDTO) {
		ExaminationPeriod ep = examinationPeriodMapper.converToEntity(examinationPeriodDTO);
		ExaminationPeriod toCreate = new ExaminationPeriod();
		toCreate = examinationPeriodService.save(ep);
		ExaminationPeriodDTO created = new ExaminationPeriodDTO();
		created = examinationPeriodMapper.converToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<ExaminationPeriodDTO> update(@Min(1) Long id,
			@Valid ExaminationPeriodDTO examinationPeriodDTO) throws NotFoundException {
		ExaminationPeriod ep = examinationPeriodService.findById(id);
		if (ep == null) {
			throw new NotFoundException(String.format("Examination period with id %d is not found!", id));
		}
		ep = examinationPeriodMapper.converToEntity(examinationPeriodDTO);
		ep.setId(id);
		ExaminationPeriod toUpdate = new ExaminationPeriod();
		toUpdate = examinationPeriodService.save(ep);
		ExaminationPeriodDTO updated = new ExaminationPeriodDTO();
		updated = examinationPeriodMapper.converToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ExaminationPeriodDTO> delete(@Min(1) Long id) throws NotFoundException {
		ExaminationPeriod ep = examinationPeriodService.findById(id);
		if (ep == null) {
			throw new NotFoundException(String.format("Examination period with id %d is not found!", id));
		}
		ExaminationPeriodDTO deleted = new ExaminationPeriodDTO();
		deleted = examinationPeriodMapper.converToDTO(ep);
		examinationPeriodService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

}
