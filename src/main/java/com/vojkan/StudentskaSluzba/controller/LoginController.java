package com.vojkan.StudentskaSluzba.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.mapper.ProfessorMapper;
import com.vojkan.StudentskaSluzba.mapper.StudentMapper;
import com.vojkan.StudentskaSluzba.model.dto.AccessToken;
import com.vojkan.StudentskaSluzba.model.dto.LoginDTO;
import com.vojkan.StudentskaSluzba.model.dto.ProfessorDTO;
import com.vojkan.StudentskaSluzba.model.dto.StudentDTO;
import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.model.security.User;
import com.vojkan.StudentskaSluzba.repository.UserRepositoryInterface;
import com.vojkan.StudentskaSluzba.security.TokenUtils;
import com.vojkan.StudentskaSluzba.service.ProfessorService;
import com.vojkan.StudentskaSluzba.service.StudentService;
import com.vojkan.StudentskaSluzba.service.UserDetailsServiceImpl;

import javassist.NotFoundException;

@RestController
public class LoginController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private ProfessorService professorService;
	
	@Autowired
	private StudentMapper studentMapper;
	
	@Autowired
	private ProfessorMapper professorMapper;
	
	@Autowired
	private UserRepositoryInterface userRepository;
	
	@Autowired
	private TokenUtils tokenUtils;

	@RequestMapping(value = "/api/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> login(@RequestBody LoginDTO loginDTO) throws NotFoundException{
		UsernamePasswordAuthenticationToken token = 
				new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(),
					loginDTO.getPassword()
				);
		Authentication authentication = authenticationManager.authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(loginDTO.getUsername());
		
		User user = userRepository.findByUsername(loginDTO.getUsername());
		
		if (user != null) {	
			AccessToken accessToken;
			String tokenString = tokenUtils.generateToken(userDetails);
			
			Student student = studentService.findByUsername(user.getUsername());
			List<String> roles = user.getUserAuthorities().stream()
					.map(x -> x.getAuthority().getName())
					.collect(Collectors.toList());
			
			if (student != null) {
				StudentDTO studentDTO = studentMapper.converToDTO(student);
				studentDTO.setRoles(roles);
				accessToken = new AccessToken(tokenString, tokenUtils.getExpirationDateFromToken(tokenString).getTime(), studentDTO);
				return new ResponseEntity<>(accessToken, HttpStatus.OK);
			}
			
			Professor professor = professorService.findByUsername(user.getUsername());
			
			if (professor != null) {
				ProfessorDTO professorDTO = professorMapper.converToDTO(professor);
				professorDTO.setRoles(roles);
				accessToken = new AccessToken(tokenString, tokenUtils.getExpirationDateFromToken(tokenString).getTime(), professorDTO);
				return new ResponseEntity<>(accessToken, HttpStatus.OK);
			}	
		}		
		
		return new ResponseEntity<>(String.format("No user found with username " + "%s", loginDTO.getUsername()), HttpStatus.NOT_FOUND);
			
	}	
	
}
