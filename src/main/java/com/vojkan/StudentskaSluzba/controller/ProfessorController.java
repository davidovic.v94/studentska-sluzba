package com.vojkan.StudentskaSluzba.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vojkan.StudentskaSluzba.controller.interfaces.ProfessorControllerInterface;
import com.vojkan.StudentskaSluzba.mapper.CourseMapper;
import com.vojkan.StudentskaSluzba.mapper.ProfessorMapper;
import com.vojkan.StudentskaSluzba.model.dto.CourseDTO;
import com.vojkan.StudentskaSluzba.model.dto.ProfessorDTO;
import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.service.CourseService;
import com.vojkan.StudentskaSluzba.service.ProfessorService;

import javassist.NotFoundException;

@RestController
@Validated
public class ProfessorController implements ProfessorControllerInterface {
	
	private final ProfessorService professorService;
	private final CourseService courseService;
	private final ProfessorMapper professorMapper;
	private final CourseMapper courseMapper;
	
	@Autowired
	public ProfessorController(ProfessorService professorService, ProfessorMapper professorMapper, 
			CourseService courseService, CourseMapper courseMapper) {
		this.professorService = professorService;
		this.courseService = courseService;
		this.professorMapper = professorMapper;
		this.courseMapper = courseMapper;
	}

	@Override
	public ResponseEntity<List<ProfessorDTO>> getProfessorsPage(Pageable page) {
		Page<Professor> professors = professorService.findAll(page);
		List<ProfessorDTO> professorDTOs = new ArrayList<>();
		professorDTOs = professors.stream()
				.map(professorMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(professorDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProfessorDTO> getProfessorById(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException {
		Professor professor = professorService.findById(id);
		if (professor == null) {
			throw new NotFoundException(String.format("Professor with id %d is not found!", id));
		}
		ProfessorDTO professorDTO = new ProfessorDTO();
		professorDTO = professorMapper.converToDTO(professor);
		return new ResponseEntity<>(professorDTO, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<ProfessorDTO>> getProfessorsByFirstName(@RequestParam String firstName, Pageable page) {
		Page<Professor> professors = professorService.findByFirstName(firstName, page);
		List<ProfessorDTO> professorDTOs = new ArrayList<>();
		professorDTOs =	professors.stream()
				.map(professorMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(professorDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<ProfessorDTO>> getProfessorsByLastName(@RequestParam String lastName, Pageable page) {
		Page<Professor> professors = professorService.findByLastName(lastName, page);
		List<ProfessorDTO> professorDTOs = new ArrayList<>();
		professorDTOs =	professors.stream()
				.map(professorMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(professorDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<CourseDTO>> getCoursesByProfessor(@Min(value = 1) @PathVariable Long id, 
			Pageable page) throws NotFoundException {
		Professor professor = professorService.findById(id);
		if (professor == null) {
			throw new NotFoundException(String.format("Professor with id %d is not found!", id));
		}
		Page<Course> courses = courseService.findByProfessor(professor, page);
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs = courses.stream()
				.map(courseMapper::converToDTO)
				.collect(Collectors.toList());
		return new ResponseEntity<>(courseDTOs, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProfessorDTO> createProfessor(@Valid @RequestBody ProfessorDTO professorDTO) {
		Professor professor = professorMapper.converToEntity(professorDTO);
		Professor toCreate = new Professor();
		toCreate = professorService.save(professor);
		ProfessorDTO created = new ProfessorDTO();
		created = professorMapper.converToDTO(toCreate);
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<ProfessorDTO> updateProfessor(@Min(value = 1) @PathVariable Long id, 
			@Valid @RequestBody ProfessorDTO professorDTO) throws NotFoundException {
		Professor professor = professorService.findById(id);
		if (professor == null) {
			throw new NotFoundException(String.format("Professor with id %d is not found!", id));
		}
		professor = professorMapper.converToEntity(professorDTO);
		professor.setId(id);
		Professor toUpdate = new Professor();
		toUpdate = professorService.save(professor);
		ProfessorDTO updated = new ProfessorDTO();
		updated = professorMapper.converToDTO(toUpdate);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ProfessorDTO> deleteProfessor(@Min(value = 1) @PathVariable Long id) 
			throws NotFoundException {
		Professor professor = professorService.findById(id);
		if (professor == null) {
			throw new NotFoundException(String.format("Professor with id %d is not found!", id));
		}
		ProfessorDTO deleted = new ProfessorDTO();
		deleted = professorMapper.converToDTO(professor);
		professorService.delete(id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

}
