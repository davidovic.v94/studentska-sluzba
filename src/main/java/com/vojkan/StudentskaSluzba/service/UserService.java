package com.vojkan.StudentskaSluzba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.model.security.User;
import com.vojkan.StudentskaSluzba.repository.UserRepositoryInterface;

@Service
public class UserService {

	@Autowired
	private UserRepositoryInterface userRepository;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private ProfessorService professorService;
	
	public Object loggedUser(String username) {
		User user = userRepository.findByUsername(username);
		
		Student student = studentService.findByUsername(user.getUsername());
		
		if (student != null) {
			return student;
		}
		
		Professor professor = professorService.findByUsername(user.getUsername());
		
		if (professor != null) {
			return professor;
		}		
		
		return null;
		
	}
	
}
