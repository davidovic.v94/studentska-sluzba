package com.vojkan.StudentskaSluzba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Notification;
import com.vojkan.StudentskaSluzba.repository.NotificationRepositoryInterface;

@Service
public class NotificationService {

	@Autowired
	private NotificationRepositoryInterface notificationRepository;
	
	public Page<Notification> findAll(Pageable page) {
		return notificationRepository.findAll(page);
	}
	
	public List<Notification> findAll() {
		return notificationRepository.findAll();
	}
	
	public Notification findById(Long id) {
		return notificationRepository.findById(id).orElse(null);
	}
	
	public Notification findByTitle(String title) {
		return notificationRepository.findByTitle(title);
	}
	
	public Notification save(Notification notification) {
		return notificationRepository.save(notification);
	}
	
	public void delete(Long id) {
		notificationRepository.deleteById(id);
	}
	
}
