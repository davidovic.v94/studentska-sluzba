package com.vojkan.StudentskaSluzba.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Exam;
import com.vojkan.StudentskaSluzba.repository.ExamRepositoryInterface;
import com.vojkan.StudentskaSluzba.utils.DateConvertor;

@Service
public class ExamService {

	@Autowired 
	private ExamRepositoryInterface examRepository;
	
	@Autowired
	private DateConvertor dateConvertor;
 	
	public List<Exam> findAll() {
		return examRepository.findAll();
	}
	
	public Page<Exam> findAll(Pageable page) {
		return examRepository.findAll(page);
	}
	
	public Exam findById(Long id) {
		return examRepository.findById(id).orElse(null);
	}
	
	public Exam save(Exam exam) {
		return examRepository.save(exam);
	}
	
	public void delete(Long id) {
		examRepository.deleteById(id);
	}
	
	public List<Exam> findByStudentId(Long id){
		return examRepository.findByStudentId(id);
	}
	
	public Page<Exam> findByStudentId(Long id, Pageable page) {
		return examRepository.findByStudentId(id, page);
	}
	
	public List<Exam> findByCourseId(Long id) {
		return examRepository.findByCourseId(id);
	}
	
	public Page<Exam> findByCourseId(Long id, Pageable page) {
		return examRepository.findByCourseId(id, page);
	}
	
	public Page<Exam> findByDate(String date, Pageable page) throws ParseException {
		List<Exam> exams = new ArrayList<Exam>();
		for(Exam e : examRepository.findAll()) {
			String eDateStr = dateConvertor.dateToString(e.getDate());
			if(eDateStr.equals(date)) {
				exams.add(e);
			}
		}
		Page<Exam> examsPage = new PageImpl<>(exams, page, exams.size());
		return examsPage;
	}
	
	public Page<Exam> findPassedExamsByStudent(Long id, Pageable page) {
		return examRepository.findPassedExamsByStudent(id, page);
	}
	
	public Page<Exam> findByGrade(int grade, Pageable page) {
		return examRepository.findByGrade(grade, page);
	}
	
	public double findStudentAvgGrade(Long id) {
		return examRepository.findStudentAvgGrade(id);
	}
	
	public int findStudentEspb(Long id) {
		return examRepository.findStudentEspb(id);
	}
	
	public void isPassed(Exam exam) {
		if ((exam.getPoints() >= 51) && (exam.getPoints() < 61)) {
			exam.setGrade(6);
			exam.setPassed(true);
		} else if ((exam.getPoints() >= 61) && (exam.getPoints() < 71)) {
			exam.setGrade(7);
			exam.setPassed(true);
		} else if ((exam.getPoints() >= 71) && (exam.getPoints() < 81)) {
			exam.setGrade(8);
			exam.setPassed(true);
		} else if ((exam.getPoints() >= 81) && (exam.getPoints() < 91)) {
			exam.setGrade(9);
			exam.setPassed(true);
		} else if ((exam.getPoints() >= 91) && (exam.getPoints() <= 100)) {
			exam.setGrade(10);
			exam.setPassed(true);
		} else {
			exam.setGrade(5);
			exam.setPassed(false);
			exam.getCourse().setRegistered(false);
		}
	}
	
}
