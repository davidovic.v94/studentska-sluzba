package com.vojkan.StudentskaSluzba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Course;
import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.model.entity.Program;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.repository.CourseRepositoryInterface;

@Service
public class CourseService {

	@Autowired
	private CourseRepositoryInterface courseRepository;
	
	public Page<Course> findAll(Pageable page){
		return courseRepository.findAll(page);
	}
	
	public List<Course> findAll(){
		return courseRepository.findAll();
	}
	
	public Course findById(Long id) {
		return courseRepository.findById(id).orElse(null);
	}
	
	public Course findByCode(String code) {
		return courseRepository.findByCode(code);
	}
	
	public Course findByName(String name) {
		return courseRepository.findByName(name);
	}
	
	public Page<Course> findBySemester(int semester, Pageable page) {
		return courseRepository.findBySemester(semester, page);
	}
	
	public Page<Course> findByEspb(int espb, Pageable page) {
		return courseRepository.findByEspb(espb, page);
	}
	
	public Page<Course> findByProgram(Program program, Pageable page) {
		return courseRepository.findByProgram(program, page);
	}
	
	public Page<Course> findByProfessor(Professor professor, Pageable page) {
		return courseRepository.findByProfessor(professor, page);
	}
	
	public Course save(Course course) {
		if (course.getProfessor().getId() == null) {
			course.setProfessor(null);
		}
		return courseRepository.save(course);
	}
	
	public void delete(Long id) {
		courseRepository.deleteById(id);
	}
	
	public boolean isValidCode(String code) {
		for(Course c : courseRepository.findAll()) {
			if (c.getCode().equals(code)) {
				return false;
			}
		}
		return true;
	}
	
	public Page<Course> findAvailableCoursesForRegistration(Student s, Pageable page) {
		return courseRepository.findAvailableCoursesForRegistration(s.getProgram().getId(), s.getYear(), s.getId(), page);
	}
	
	public Page<Course> findRegistratedCourses(Student s, Pageable page) {
		return courseRepository.findRegistratedCourses(s.getProgram().getId(), s.getYear(), s.getId(), page);
	}
	
}
