package com.vojkan.StudentskaSluzba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Professor;
import com.vojkan.StudentskaSluzba.repository.ProfessorRepositoryInterface;

@Service
public class ProfessorService {

	@Autowired
	private ProfessorRepositoryInterface professorRepository;
	
	public List<Professor> findAll() {
		return professorRepository.findAll();
	}
	
	public Page<Professor> findAll(Pageable page) {
		return professorRepository.findAll(page);
	}
	
	public Professor findById(Long id) {
		return professorRepository.findById(id).orElse(null);
	}
	
	public Professor findByUsername(String username) {
		return professorRepository.findByUsername(username);
	}
		
	public Page<Professor> findByFirstName(String firstName, Pageable page) {
		return professorRepository.findByFirstName(firstName, page);
	}
	
	public Page<Professor> findByLastName(String lastName, Pageable page) {
		return professorRepository.findByLastName(lastName, page);
	}
	
	public Professor save(Professor professor) {
		return professorRepository.save(professor);
	}
	
	public void delete(Long id) {
		professorRepository.deleteById(id);
	}
	
}
