package com.vojkan.StudentskaSluzba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Program;
import com.vojkan.StudentskaSluzba.model.entity.Student;
import com.vojkan.StudentskaSluzba.repository.StudentRepositoryInterface;

@Service
public class StudentService {

	@Autowired
	private StudentRepositoryInterface studentRepository;
	
	public List<Student> findAll() {
		return studentRepository.findAll();
	}
	
	public Page<Student> findAll(Pageable page) {
		return studentRepository.findAll(page);
	}
	
	public Student findById(Long id) {
		return studentRepository.findById(id).orElse(null);
	}
	
	public Student findByUsername(String username) {
		return studentRepository.findByUsername(username);
	}
		
	public Page<Student> findByFirstName(String firstName, Pageable page) {
		return studentRepository.findByFirstName(firstName, page);
	}
	
	public Page<Student> findByLastName(String lastName, Pageable page) {
		return studentRepository.findByLastName(lastName, page);
	}
	
	public Student findByCardNumber(String cardNumber) {
		return studentRepository.findByCardNumber(cardNumber);
	}
	
	public Page<Student> findByProgram(Program program, Pageable page) {
		return studentRepository.findByProgram(program, page);
	}
	
	public Page<Student> findByYear(int year, Pageable page) {
		return studentRepository.findByYear(year, page);
	}
	
	public Student save(Student student) {
		return studentRepository.save(student);
	}
	
	public void delete(Long id) {
		studentRepository.deleteById(id);
	}
	
	public boolean isValidCardNumber(String cardNumber) {
		for(Student s : studentRepository.findAll()) {
			if(s.getCardNumber().equals(cardNumber)) {
				return false;
			}
		}
		return true;
	}
	
}
