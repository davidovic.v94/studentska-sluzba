package com.vojkan.StudentskaSluzba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.Program;
import com.vojkan.StudentskaSluzba.repository.ProgramRepositoryInterface;

@Service
public class ProgramService {
	
	@Autowired
	private ProgramRepositoryInterface programRepository;

	public Page<Program> findAll(Pageable page) {
		return programRepository.findAll(page);
	}
	
	public List<Program> findAll() {
		return programRepository.findAll();
	}
	
	public Program findById(Long id) {
		return programRepository.findById(id).orElse(null);
	}
	
	public Program findByName(String name) {
		return programRepository.findByName(name);
	}
	
	public Program findByTotalEspb(int espb) {
		return programRepository.findByTotalEspb(espb);
	}
	
	public Program save(Program program) {
		return programRepository.save(program);
	}
	
	public void delete(Long id) {
		programRepository.deleteById(id);
	}
	
	public boolean isValidName(String name) {
		for(Program p : programRepository.findAll()) {
			if (p.getName().equals(name)) {
				return false;
			}
		}
		return true;
	}
	
}
