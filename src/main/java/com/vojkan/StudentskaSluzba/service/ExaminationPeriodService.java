package com.vojkan.StudentskaSluzba.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vojkan.StudentskaSluzba.model.entity.ExaminationPeriod;
import com.vojkan.StudentskaSluzba.repository.ExaminationPeriodRepositoryInterface;

@Service
public class ExaminationPeriodService {

	@Autowired
	private ExaminationPeriodRepositoryInterface examinationPeriodRepositoryInterface;	
	
	public List<ExaminationPeriod> findAll() {
		return examinationPeriodRepositoryInterface.findAll();
	}
	
	public Page<ExaminationPeriod> findAll(Pageable page) {
		return examinationPeriodRepositoryInterface.findAll(page);
	}
	
	public ExaminationPeriod findById(Long id) {
		return examinationPeriodRepositoryInterface.findById(id).orElse(null);
	}
	
	public ExaminationPeriod findByName(String name) {
		return examinationPeriodRepositoryInterface.findByName(name);
	}
	
	public ExaminationPeriod save(ExaminationPeriod examinationPeriod) {
		return examinationPeriodRepositoryInterface.save(examinationPeriod);
	}
	
	public void delete(Long id) {
		examinationPeriodRepositoryInterface.deleteById(id);
	}
	
	public boolean isRegistrationPeriod() {
		Date currentDate = new Date();
		List<ExaminationPeriod> examinationPeriods = new ArrayList<>();
		examinationPeriods = findAll();
		for(ExaminationPeriod ep : examinationPeriods) {
			if (!ep.getRegistrationStart().after(currentDate) && !ep.getRegistrationEnd().before(currentDate)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isExaminationPeriod() {
		Date currentDate = new Date();
		List<ExaminationPeriod> examinationPeriods = new ArrayList<>();
		examinationPeriods = findAll();
		for(ExaminationPeriod ep : examinationPeriods) {
			if (!ep.getStart().after(currentDate) && !ep.getEnd().before(currentDate)) {
				return true;
			}
		}
		return false;
	}
	
}
